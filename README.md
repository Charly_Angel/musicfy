# Musicfy

aplicación de escritorio para reproducir musica creado con react y electron

## `Getting started`

copy .env.example file to .env

```bash
cp .env.example .env
```

add environment variables

```shell
###> firebase ###
REACT_APP_API_KEY='api-key-firebase'
REACT_APP_AUTH_DOMAIN='auth-domain-firebase'
REACT_APP_PROJECT_ID='project-id-firebase'
REACT_APP_STORAGE_BUCKET='storage-bucket-firebase'
REACT_APP_MESSAGING_SENDER_ID='messaging-sender-id-firebase'
REACT_APP_APP_ID='app-id-firebase'
### firebase <###
```

install dependencies

```bash
yarn install
```

## `Development`

```bash
yarn electron-dev
```

## `Builder`

builder electron pack

```bash
## builder for windows
yarn electron-pack --win

## builder for mac
yarn electron-pack --mac

## builder for linux
yarn electron-pack --linux
```

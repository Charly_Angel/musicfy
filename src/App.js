import { Notification } from 'components/Notification';
import {AuthProvider} from 'context/auth';
import { AppRoutes } from 'routes/AppRoutes';
import 'app';

function App() {
  return (
    <AuthProvider>
      <Notification />
      <AppRoutes />
    </AuthProvider>
  );
}

export default App;

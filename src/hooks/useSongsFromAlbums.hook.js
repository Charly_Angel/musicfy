import { useState, useEffect } from "react";
import { getSongsFromAlbum } from "services/songs";
import map from "lodash/map";
import size from "lodash/size";

export function useSongsFromAlbums(albums = []) {
  const [songs, setSongs] = useState(null);
  const existAlbums = size(albums) > 0;

  useEffect(() => {
    if (existAlbums) {
      const songsList = [];
      (async () => {
        await Promise.all(
          map(albums, async (album) => {
            await getSongsFromAlbum(album.id)
              .then((response) => {
                songsList.push(...response);
              })
              .catch(() => {});
          })
        );
        setSongs(songsList);
      })();
    }
  }, [existAlbums, albums]);

  return songs;
}

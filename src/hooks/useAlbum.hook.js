import { useState, useEffect } from "react";
import { getAlbum } from "services/album";

export function useAlbum({ id }) {
  const [album, setAlbum] = useState(null);
  useEffect(() => {
    if (id) {
      getAlbum(id)
        .then(setAlbum)
        .catch(() => setAlbum(null));
    }
  }, [id]);

  return album;
}

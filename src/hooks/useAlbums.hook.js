import { useState, useEffect } from "react";
import { getAlbums } from "services/album";

export function useAlbums() {
  const [albums, setAlbums] = useState([]);

  useEffect(() => {
    getAlbums()
      .then((response) => {
        setAlbums(response);
      })
      .catch(() => setAlbums([]));
  }, []);

  return albums;
}

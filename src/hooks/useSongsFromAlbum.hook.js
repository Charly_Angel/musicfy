import { useState, useEffect } from "react";
import { getSongsFromAlbum } from "services/songs";

export function useSongsFromAlbum({ albumId }) {
  const [songs, setSongs] = useState(null);
  useEffect(() => {
    if (albumId) {
      getSongsFromAlbum(albumId)
        .then(setSongs)
        .catch(() => setSongs(null));
    }
  }, [albumId]);

  return songs;
}

import { useState, useEffect } from "react";
import { getAlbumFromArtist } from "services/album";

export function useAlbumsFromArtist({ artistId }) {
  const [albums, setAlbums] = useState([]);
  useEffect(() => {
    if (artistId) {
      getAlbumFromArtist(artistId)
        .then(setAlbums)
        .catch(() => setAlbums([]));
    }
  }, [artistId]);

  return albums;
}

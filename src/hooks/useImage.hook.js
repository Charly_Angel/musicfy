import { useState, useEffect } from "react";
import { getImage } from "services/storage";

export function useImage({ folder, imageName }) {
  const [imageUrl, setImageUrl] = useState(null);

  useEffect(() => {
    if (imageName && folder) {
      getImage({ folder, imageName })
        .then((response) => {
          setImageUrl(response);
        })
        .catch(() => setImageUrl(null));
    }
  }, [folder, imageName]);

  return imageUrl;
}

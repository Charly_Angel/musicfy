import { useState } from 'react';

export const usePassword = () => {
  const [show, setShow] = useState(false);

  const showPassword = () => setShow(!show);

  return [show, showPassword];
};

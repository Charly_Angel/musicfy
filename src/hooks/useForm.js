import { useState } from 'react';

export const useForm = (initialState = {}) => {
  const [data, setData] = useState(initialState);

  const handleChange = (event) => {
    setData({ ...data, [event.target.name]: event.target.value });
  };

  const reset = () => setData(initialState);

  return {
    data,
    handleChange,
    reset,
  };
};

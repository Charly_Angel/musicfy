import { useState, useEffect } from "react";
import { getSongs } from "services/songs";

export function useSongs() {
  const [songs, setSongs] = useState([]);

  useEffect(() => {
    getSongs()
      .then((response) => {
        setSongs(response);
      })
      .catch(() => setSongs([]));
  }, []);

  return songs;
}

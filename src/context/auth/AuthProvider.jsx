import { createContext, useMemo } from "react";
import { useAuthProvider } from "./useAuthProvider.hook";

const INITIAL_STATE = {
  isAuthenticated: false,
  user: null,
  isAdmin: false,
  error: false,
  loading: false,
  login: async ({ email, password }) => {},
  logout: () => {},
  onReload: () => {},
};

export const AuthContext = createContext(INITIAL_STATE);

export function AuthProvider({ children }) {
  const state = useAuthProvider();
  const auth = useMemo(() => state, [state]);
  return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
}

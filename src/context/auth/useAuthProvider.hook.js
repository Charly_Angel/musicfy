import { useReducer, useEffect } from "react";
import { signIn, signOut } from "services/auth";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { INITIAL_STATE, actions, authReducer } from "./auth.reducer";
import { isAdmin } from "services/user";

export function useAuthProvider() {
  const [state, dispatch] = useReducer(authReducer, INITIAL_STATE);

  useEffect(() => {
    dispatch(actions.authenticateRequest());
    const auth = getAuth();
    onAuthStateChanged(auth, async (user) => {
      if (!user?.emailVerified) {
        auth.signOut();
        dispatch(actions.authenticateFailure());
      } else {
        const isUserAdmin = await isAdmin({ uid: user.uid });
        dispatch(actions.authenticateSuccess({ user, isAdmin: isUserAdmin }));
      }
    });
  }, []);

  const logout = () => {
    signOut();
    dispatch(actions.logout());
  };

  const login = async ({ email, password }) => {
    dispatch(actions.authenticateRequest());
    try {
      const { error, user } = await signIn({ email, password });
      if (error) {
        dispatch(actions.authenticateFailure());
      } else {
        const isUserAdmin = await isAdmin({ uid: user.uid });
        dispatch(actions.authenticateSuccess({ user, isAdmin: isUserAdmin }));
      }
    } catch (error) {
      dispatch(actions.authenticateFailure());
    }
  };

  const onReload = async () => {
    dispatch(actions.authenticateRequest());
    const auth = getAuth();
    onAuthStateChanged(auth, async (user) => {
      if (!user?.emailVerified) {
        auth.signOut();
        dispatch(actions.authenticateFailure());
      } else {
        const isUserAdmin = await isAdmin({ uid: user.uid });
        dispatch(actions.authenticateSuccess({ user, isAdmin: isUserAdmin }));
      }
    });
  };

  return { ...state, logout, login, onReload };
}

import { Switch, Route, Redirect } from 'react-router-dom';
import { Auth } from "pages/Auth"
import Login from 'pages/Auth/Login';
import Register from 'pages/Auth/Register';

export const AuthRoutes = () => {
  return (
    <Switch>
        <Route exact path='/auth' children={<Auth />} />
        <Route exact path='/auth/login' children={<Login />} />
        <Route exact path='/auth/register' children={<Register />} />
        <Redirect to='/auth' />
    </Switch>
  )
}

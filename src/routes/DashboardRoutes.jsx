import { Switch, Route, Redirect } from "react-router-dom";
import { Artist } from "pages/Artist";
import { Artists } from "pages/Artists";
import { Home } from "pages/Home";
import { Albums } from "pages/Albums";
import { Album } from "pages/Album";
import { Settings } from "pages/Settings";
import { Logged } from "layouts/Logged";

export function DashboardRoutes() {
  return (
    <Logged>
      <Switch>
        <Route exact path="/" children={<Home />} />
        <Route exact path="/artists" children={<Artists />} />
        <Route exact path="/artist/:id" children={<Artist />} />
        <Route exact path="/albums" children={<Albums />} />
        <Route exact path="/album/:id" children={<Album />} />
        <Route exact path="/settings" children={<Settings />} />
        <Redirect to="/" />
      </Switch>
    </Logged>
  );
}

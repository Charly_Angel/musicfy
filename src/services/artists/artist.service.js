import {
  getFirestore,
  doc,
  getDoc,
  addDoc,
  collection,
} from "firebase/firestore";
import { toast } from "react-toastify";
import { uploadFile } from "services/storage";

export async function getArtist(id) {
  const db = getFirestore();
  const docRef = doc(db, "artists", id);

  const snapshot = await getDoc(docRef);
  const existsSnapshot = snapshot.exists();

  const data = existsSnapshot ? { id: snapshot.id, ...snapshot.data() } : false;

  return data;
}

export async function addArtist(data) {
  try {
    const db = getFirestore();
    const banner = await uploadFile({ folder: "artist", file: data.banner });
    delete data.banner;
    await addDoc(collection(db, "artists"), { ...data, banner });
    toast.success("Artista añadido correctamente");
    return true;
  } catch (error) {
    toast.error("Error al añadir el artista");
    return false;
  }
}

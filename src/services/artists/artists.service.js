import { collection, getDocs, getFirestore } from "firebase/firestore";

export async function getArtists() {
  const db = getFirestore();
  const ref = collection(db, "artists");

  const snapshot = await getDocs(ref);
  const data = snapshot.docs.map((doc) => {
    return { ...doc.data(), id: doc.id };
  });

  return data || [];
}

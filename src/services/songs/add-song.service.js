import { getFirestore, addDoc, collection } from "firebase/firestore";
import { toast } from "react-toastify";
import { uploadFile } from "services/storage";

export async function addSong(data) {
  try {
    const db = getFirestore();
    const fileName = await uploadFile({ folder: "songs", file: data.song });
    delete data.song;
    await addDoc(collection(db, "songs"), { ...data, fileName });
    toast.success("Canción subida correctamente");
    return true;
  } catch (error) {
    toast.error("Error al subir la canción");
    return false;
  }
}

import { getDoc, getFirestore, doc } from "firebase/firestore";

export async function isAdmin({ uid }) {
  const db = getFirestore();
  const docRef = doc(db, "admins", uid);

  const admin = await getDoc(docRef);
  return admin.exists();
}

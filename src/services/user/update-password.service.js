import { getAuth } from "firebase/auth";
import { updatePassword as updatePasswordFirebase } from "firebase/auth";
import { toast } from "react-toastify";
import { reauthenticate } from "services/auth";

export async function updatePassword({ newPassword, oldPassword }) {
  try {
    const isReauthenticate = await reauthenticate({ password: oldPassword });
    if (isReauthenticate) {
      const auth = getAuth();
      await updatePasswordFirebase(auth.currentUser, newPassword);
      toast.success("Contraseña actualizada");
      return true;
    } else {
      toast.error("La contraseña es incorrecta");
      return false;
    }
  } catch (error) {
    toast.error("Error al actualizar la contraseña");
    return false;
  }
}

import { getAuth, updateProfile } from "firebase/auth";
import { toast } from "react-toastify";
import { getImage, uploadFile } from "services/storage";

export async function uploadAvatar(avatar) {
  try {
    const imageName = await uploadFile({ folder: "avatar", file: avatar });
    const photoURL = await getImage({ folder: "avatar", imageName });
    const auth = getAuth();
    await updateProfile(auth.currentUser, { photoURL });
    toast.success("imagen subida correctamente");
  } catch (error) {
    toast.error("error al subir la imagen");
  }
}

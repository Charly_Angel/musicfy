import { getStorage, ref, uploadBytes } from "firebase/storage";
import { v4 as uuidv4 } from "uuid";

export async function uploadFile({ folder, file }) {
  try {
    const storage = getStorage();
    const fileName = uuidv4();
    const imageRef = ref(storage, `${folder}/${fileName}`);
    await uploadBytes(imageRef, file);
    return fileName;
  } catch (error) {
    return false;
  }
}

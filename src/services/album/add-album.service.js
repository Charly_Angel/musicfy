import { getFirestore, addDoc, collection } from "firebase/firestore";
import { toast } from "react-toastify";
import { uploadFile } from "services/storage";

export async function addAlbum(data) {
  try {
    const db = getFirestore();
    const banner = await uploadFile({ folder: "album", file: data.banner });
    delete data.banner;
    await addDoc(collection(db, "albums"), { ...data, banner });
    toast.success("Album añadido correctamente");
    return true;
  } catch (error) {
    toast.error("Error al añadir el Album");
    return false;
  }
}

export { getAlbums, getAlbum } from "./album.service";
export { getAlbumFromArtist } from "./get-albums-from-artist.service";
export { addAlbum } from "./add-album.service";

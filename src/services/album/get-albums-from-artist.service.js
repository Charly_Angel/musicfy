import {
  getFirestore,
  collection,
  where,
  getDocs,
  query,
} from "firebase/firestore";

export async function getAlbumFromArtist(artistId) {
  const db = getFirestore();
  const ref = collection(db, "albums");

  const q = query(ref, where("artist", "==", artistId));

  const snapshot = await getDocs(q);
  const data = snapshot.docs.map((doc) => {
    return { ...doc.data(), id: doc.id };
  });

  return data || [];
}

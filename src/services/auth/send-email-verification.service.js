import { sendEmailVerification } from "firebase/auth";
import { toast } from "react-toastify";

export async function userVerified({userFirebase}) {
  try {
    await sendEmailVerification(userFirebase);
    toast.success("Se ha enviado el email para verificar tu cuenta.")
  } catch (error) {
    toast.error("Error al enviar el email de verificación.");
  }
}

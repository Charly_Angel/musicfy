import { createUserWithEmailAndPassword, getAuth } from 'firebase/auth';
import { updateUsername } from 'services/user';
import { userVerified } from 'services/auth';
import { handleErrorFirebase } from 'utils/errors-firebase.utility';

export async function createUser({email, password, username}){
    try {
        const auth = getAuth();
        await createUserWithEmailAndPassword(auth, email, password);
        const userFirebase = auth.currentUser;
        await updateUsername({userFirebase, username});
        await userVerified({userFirebase});
        auth.signOut();
        return true;
    } catch (error) {
        const code = error.code;
        handleErrorFirebase(code);
        return false;
    }
}
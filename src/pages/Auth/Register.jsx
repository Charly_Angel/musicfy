import { RegisterForm } from 'components/Auth/RegisterForm';

import BackgroundAuth from 'assets/jpg/background-auth.jpg';
import LogoNameWithe from 'assets/png/logo-name-white.png';
import './styles.scss';import React from 'react'

export default function Register() {
    return (
        <div
          className='auth'
          style={{ backgroundImage: `url('${BackgroundAuth}')` }}
        >
          <div className='auth__dark' />
    
          <div className='auth__box'>
            <div className='auth__box-logo'>
              <img src={LogoNameWithe} alt='Musicfy' />
            </div>
            <RegisterForm />
          </div>
        </div>
      );
}

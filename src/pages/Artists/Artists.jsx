import { Link } from "react-router-dom";
import { Grid } from "semantic-ui-react";
import { map } from "lodash";
import { useImage } from "hooks/useImage.hook";
import { useArtists } from "hooks/useArtists.hook";
import "./styles.scss";

export const Artists = () => {
  const artists = useArtists();

  return (
    <div className="artists">
      <h1>Artistas</h1>
      <Grid>
        {map(artists, (artist) => (
          <Grid.Column key={artist.id} mobile={8} tablet={4} computer={3}>
            <Artist {...artist} />
          </Grid.Column>
        ))}
      </Grid>
    </div>
  );
};

function Artist({ name, banner, id }) {
  const avatar = useImage({ folder: "artist", imageName: banner });
  return (
    <Link to={`artist/${id}`}>
      <div className="artists__item">
        <div
          className="avatar"
          style={{ backgroundImage: `url('${avatar}')` }}
        />
        <h3>{name}</h3>
      </div>
    </Link>
  );
}

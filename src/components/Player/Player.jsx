import { Grid, Progress, Icon, Input, Image } from "semantic-ui-react";
import ReactPlayer from "react-player";
import { usePlayer } from "hooks/usePlayer.hook";
import "./styles.scss";

export const Player = () => {
  const {
    song,
    playing,
    playerSeconds,
    totalSeconds,
    volume,
    onStart,
    onStop,
    onProgress,
    onChangeVolume,
  } = usePlayer();

  return (
    <div className="player">
      <Grid>
        <Grid.Column width={4} className="left">
          <Image src={song?.image} /> {song?.name}
        </Grid.Column>
        <Grid.Column width={8} className="center">
          <div className="controls">
            {playing ? (
              <Icon onClick={onStop} name="pause circle outline" />
            ) : (
              <Icon onClick={onStart} name="play circle outline" />
            )}
          </div>
          <Progress
            size="tiny"
            progress="value"
            value={playerSeconds}
            total={totalSeconds}
          />
        </Grid.Column>
        <Grid.Column width={4} className="right">
          <Input
            label={<Icon name="volume up" />}
            type="range"
            min={0}
            max={1}
            step={0.01}
            name="volume"
            onChange={(e, data) => onChangeVolume(Number(data.value))}
            value={volume}
          />
        </Grid.Column>
      </Grid>
      <ReactPlayer
        className="react-player"
        url={song?.url}
        playing={playing}
        height={0}
        width={0}
        volume={volume}
        onProgress={(e) => onProgress(e)}
      />
    </div>
  );
};

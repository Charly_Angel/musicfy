import { Button, Icon } from "semantic-ui-react";
import { Form, Formik } from "formik";
import { Link } from "react-router-dom";
import * as Yup from "yup";
import { Input } from "components/common/Form";
import { usePassword } from "hooks/usePassword";
import { useAuth } from "hooks/useAuth.hook";
import "./styles.scss";

export function LoginForm() {
  const [showPassword, handleShowPassword] = usePassword();
  const { login } = useAuth();
  return (
    <div className="login-form">
      <h1>Música para todos.</h1>
      <Formik
        initialValues={{ email: "", password: "" }}
        onSubmit={async ({ email, password }) => {
          await login({ email, password });
        }}
        validationSchema={Yup.object({
          email: Yup.string()
            .email("El email no es válido")
            .required("El email es requerido"),
          password: Yup.string().required("La contraseña es requerida"),
        })}
      >
        {({ isSubmitting }) => (
          <Form className="login-form">
            <Input
              name="email"
              type="email"
              placeholder="Email"
              icon="mail outline"
            />
            <Input
              name="password"
              type={showPassword ? "text" : "password"}
              placeholder="Contraseña"
              icon={
                showPassword ? (
                  <Icon
                    name="eye slash outline"
                    link
                    onClick={handleShowPassword}
                  />
                ) : (
                  <Icon name="eye" link onClick={handleShowPassword} />
                )
              }
            />
            <Button
              type="submit"
              loading={isSubmitting}
              disabled={isSubmitting}
            >
              Iniciar sesión
            </Button>
          </Form>
        )}
      </Formik>

      <div className="login-form__options">
        <p>
          <Link to="/auth">Regresar</Link>
        </p>
        <p>
          ¿No tienes una cuenta?
          <Link to="/auth/register">
            <span>Regístrate aquí</span>
          </Link>
        </p>
      </div>
    </div>
  );
}

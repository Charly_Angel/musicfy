import { Button, Icon } from "semantic-ui-react";
import { Formik, Form } from "formik";
import { Link, useHistory } from "react-router-dom";
import * as Yup from "yup";
import { Input } from "components/common/Form";
import { usePassword } from "hooks/usePassword";
import { createUser } from "services/auth";
import "./styles.scss";

export function RegisterForm() {
  const history = useHistory();
  const [showPassword, handleShowPassword] = usePassword();
  return (
    <div className="register-form">
      <h1>Empieza a escuchar con una cuenta de Musicfy gratis.</h1>
      <Formik
        initialValues={{ email: "", password: "", username: "" }}
        validationSchema={Yup.object({
          email: Yup.string()
            .email("El email no es válido")
            .required("El email es requerido"),
          password: Yup.string()
            .min(8, "La contraseña debe tener al menos 8 caracteres")
            .required("La contraseña es requerida"),
          username: Yup.string()
            .min(3, "El nombre de usuario debe tener al menos 3 caracteres")
            .required("El nombre de usuario es requerido"),
        })}
        onSubmit={async ({email, password, username}) => {
          const isUserCreated = await createUser({email, password, username});
          if(isUserCreated){
            history.push('/auth/login');
          }
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <Input
              name="email"
              type="email"
              placeholder="Email"
              icon="mail outline"
            />
            <Input
              name="password"
              type={showPassword ? "text" : "password"}
              placeholder="Contraseña"
              icon={
                showPassword ? (
                  <Icon
                    name="eye slash outline"
                    link
                    onClick={handleShowPassword}
                  />
                ) : (
                  <Icon name="eye" link onClick={handleShowPassword} />
                )
              }
            />
            <Input
              type="text"
              name="username"
              placeholder="¿Como deberíamos llamarte?"
              icon="user circle outline"
            />
            <Button
              type="submit"
              loading={isSubmitting}
              disabled={isSubmitting}
            >
              Crear cuenta
            </Button>
          </Form>
        )}
      </Formik>

      <div className="register-form__options">
        <p><Link to='/auth'>Regresar</Link></p>
        <p>
          {"¿Ya tienes una cuenta? "}
          <Link to='/auth/login'><span>Iniciar sesión</span></Link>
        </p>
      </div>
    </div>
  );
}

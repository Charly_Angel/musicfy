import { useImage } from "hooks/useImage.hook";
import "./styles.scss";

export const BannerHome = () => {
  const banner = useImage({ folder: "other", imageName: "banner-home.jpg" });
  return !banner ? null : (
    <div
      className="banner-home"
      style={{ backgroundImage: `url('${banner}')` }}
    />
  );
};

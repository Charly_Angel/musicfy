import React from 'react';
import { ToastContainer } from 'react-toastify';

import './styles.scss';

export const Notification = () => {
  return (
    <ToastContainer
      position='top-center'
      autoClose={5000}
      hideProgressBar
      newestOnTop
      closeOnClick
      rtl={false}
      draggable
      pauseOnHover={true}
    />
  );
};
